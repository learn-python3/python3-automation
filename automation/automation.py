from selenium import webdriver

driver = webdriver.Chrome('./chromedriver')

driver.maximize_window()
driver.get('https://www.seleniumeasy.com/test/basic-first-form-demo.html')

assert 'Selenium Easy Demo - Simple Form to Automate using Selenium' in driver.title
# assert 'Selenium Easy Demo' in driver.body

show_message_button = driver.find_element_by_class_name('btn-default')
print(show_message_button.get_attribute('innerHTML'))

# Check if `Show Message` strings exist within the HTML Document.
assert 'Show Message' in driver.page_source

user_message = driver.find_element_by_id('user-message')
user_message.clear()

# A pop up is displayed and hinders continuation of automation test.
popup = driver.find_element_by_id('at-cv-lightbox')
if popup:
    driver.execute_script("""
    let element = arguments[0]
    element.parentNode.removeChild(element)
    """, popup)

# An alternative would be to import module `time` and:
# time.sleep(5)
# popup = chrome_browswer.find_element_by_id('at-cv-lightbox-close')
# popup.click()

# Send keys simulates typing (in the field).
customMessage = 'This is a automatically generated message'
user_message.send_keys(customMessage)
show_message_button.click()
output_message = driver.find_element_by_id('display')

assert customMessage in output_message.text

# Close has a bug, in which case it does not close the browser instance
# and might to be called twice to fix that.
# In case that it closed with the first try, the second call causes
# an exception. Thus wrap it in a try block.
driver.close()
try:
    driver.close()
except:
    pass

# To close ALL instances call `quit` method.
# driver.quit()
